### Setup 

Works more or less

This is a playbook that needs to be at least 3 roles. I've tried to stick to ansibles best practices.

## Running with Vagrant

`vagrant up` 

That will get you an ubuntu trusty install with apache and postgresql and a copy of ojs 2.4.6

In your cloned repo add the following files

A copy of the `config.inc.php`
A copy of the most current database dump
A copy of the /var/www/ojsfiles

Connect to your vagrant vm 

`vagrant ssh`

Restore into the created database with the following command

`sudo -u postgres psql ojs244 < /vagrant/nameofdb_dump.sql`

Restart the database server

`sudo /usr/sbin/service postgresql restart`

Restart the web server

`sudo /usr/sbin/service apache2 restart`

Change the path of 

files_dir to

files_dir = /vagrant/ojsfiles

You will need to update the database schema. 

`cd /var/www/html/ojs`

`sudo -s`

`php tools/upgrade.php upgrade`

This will upgrade the database schema. 
